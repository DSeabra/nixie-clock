import RPi.GPIO as GPIO
import datetime
import time
from enum import IntEnum
from nixie_helpers import *

# Constants (these are meant to be immutable)
SLEEP_TIME = 30.  # Number of milliseconds to sleep every cycle
CYCLES_PER_SECOND = 1000. / SLEEP_TIME
SECONDS_PER_MINUTE = 60.
BUTTON_IO = 10    # IO pin on the RPi header connected to the button
PIR_IO = 8        # IO pin on the RPi header connected to the PIR sensor output

# Button needs to be held down for a 15th of a second for it to count
#   as a button press
BUTTON_PRESS_INIT_VAL = CYCLES_PER_SECOND / 15

# Button needs to be held down for 3 seconds to set the clock to sleep
#   mode or wake it
BUTTON_HOLD_INIT_VAL = CYCLES_PER_SECOND * 3

# PIR sensor must not detect movement for 1 minute to power down the
#   tube display
PIR_DETECT_INIT_VAL = 1 * CYCLES_PER_SECOND * SECONDS_PER_MINUTE

# These are the different modes that the clock supports. I use an IntEnum
#   class so that I can cycle through them easily when the button is pressed
Mode = IntEnum('Mode', 'TIME TEMP AQI ALTERNATE')

# Update the external temperature every fifteen minutes
EXT_TEMP_UPDATE_SECONDS = 15 * SECONDS_PER_MINUTE

# Update the CPU temperature every 60 seconds
INT_TEMP_UPDATE_SECONDS = 60

def initGPIOs():
    ''' The clock has six digits, as follows:
           H1  H2     M1  M2    S1  S2

        This function first initializes one list per digit that indicates
            which header pins the four binary signals that drive the decoders
            are connected to in order from MSB to LSB.

        This function builds a list of these lists and initializes the IO
        pins by configuring them as an output and outputting a HIGH on them,
        as all HIGHs on the digits results in a display that is turned off.

        Then it initializes the button and PIR IOs as inputs.

        The function the returns the list of IOs
    '''
    dig_H1 = [11, 7, 5, 3]
    dig_H2 = [16, 18, 15, 13]
    dig_M1 = [22, 24, 21, 19]
    dig_M2 = [26, 32, 12, 23]
    dig_S1 = [35, 33, 31, 29]
    dig_S2 = [36, 38, 40, 37]

    # Defines how we reference the Raspberry Pi pin names - GPIO.BOARD
    #   defines them by pin number counting from the header, so, for example,
    #   GPIO.setup(3, GPIO.OUT) would set up pin three of the header as an
    #   output
    GPIO.setmode(GPIO.BOARD)

    list_digit_IOs = [dig_H1, dig_H2, dig_M1, dig_M2, dig_S1, dig_S2]

    for lst in list_digit_IOs:
        for io in lst:
            GPIO.setup(io, GPIO.OUT)

            # Initialize to high to make sure the tubes are off at the beginning
            GPIO.output(io, GPIO.HIGH)

    GPIO.setup(BUTTON_IO, GPIO.IN, pull_up_down=GPIO.PUD_UP)
    GPIO.setup(PIR_IO, GPIO.IN)

    return list_digit_IOs

def checkButton(button_press_timer, button_hold_timer):
    ''' This function checks the status of the button and does a little
    debouncing on it. A button press is signalled if the button is held when
    button_press_timer = 0 and a button hold is reported when button_hold_timer = 0

    The function returns a tuple with the updated button hold and press timers
    as well as two booleans that indicate if a press or hold was detected:
        (press_timer, hold_timer, BUTTON_PRESSED, BUTTON_HELD)
    '''

    if GPIO.input(BUTTON_IO) == GPIO.LOW:
        # NOTE: You can do button_press_timer = max(0, button_press_timer - 1)
        #   or reset the counter to a different value when a zero is detected
        #   if you want to do continuous button presses when the button is held.
        #   Right now, the button will need to be released and then pressed
        #   again to get two events in a row

        # If the button is pressed (IO is LOW), decrement the counters
        button_press_timer = max(-1, button_press_timer - 1)
        button_hold_timer = max(-1, button_hold_timer - 1)

    else:
        # Otherwise it was released, so lets set them back to their default
        #   values
        button_press_timer = BUTTON_PRESS_INIT_VAL
        button_hold_timer = BUTTON_HOLD_INIT_VAL

    button_pressed = button_press_timer == 0
    button_held = button_hold_timer == 0

    return (button_press_timer, button_hold_timer, button_pressed, button_held)


def checkPIR(pir_timer):
    ''' This function checks the status of the PIR sensor output and reports
    whether no motion was detected for PIR_DETECT_INIT_VAL cycles to turn off
    the display. It returns a tuple with the updated value of the timer and a
    boolean that reports whether it the motion timeout was triggered.
    '''

    if GPIO.input(PIR_IO) == GPIO.LOW:
        # If the PIR sensor IO is LOW, then no motion was detected for this
        #   period, so decrement the counter
        pir_timer = max(0, pir_timer - 1)
    else:
        # Otherwise we detected motion this cycle, so let's reset the counter
        pir_timer = PIR_DETECT_INIT_VAL

    return (pir_timer, pir_timer == 0)

def getDisplayString(mode, is_asleep, value):
    """ This function takes in the current clock mode and value(s) to display and
    creates the six digit string required to display it on the six digit nixie
    tube clock. The string is returned to be used by updateDisplay.

    The parameter value is expected to be:
       A datetime.datetime object if mode is Mode.TIME
       An int tuple of (external temp, internal temp) if mode is Mode.TEMP
       An int tuple of (aqi_class, aqi_number) if mode is Mode.AQI

    NOTE: If mode is none of the above, this function returns "777777" as
    an error string.

    NOTE: I use the character A to turn off a particular digit. If the clock
    is in sleep mode, the whole display is turned off (all As).
    """

    # Turn off the display if the clock is asleep
    if is_asleep:
        return "A" * 6

    if mode == Mode.TIME:
        # Display the time
        time = value
        hours_str = "{0:0=2d}".format(time.hour)
        minutes_str = "{0:0=2d}".format(time.minute)
        seconds_str = "{0:0=2d}".format(time.second)

        return (hours_str + minutes_str + seconds_str)

    if mode == Mode.TEMP:
        # Combine the internal and external temperatures into one display
        #   string with the external temperature left justified and the
        #   internal temperature right justitified
        i_temp_str = "{0:A=6d}".format(value[0])
        e_temp_str = str(value[1])
        temp_str = e_temp_str + i_temp_str[len(e_temp_str):6]

        return temp_str

    if mode == Mode.AQI:
        # Combine the AQI category number and the AQI number into one
        #   display string with the AQI category number left justified and
        #   the AQI number right justified
        aqi_num_str = "{0:A=6d}".format(value[1])
        aqi_cat_str = str(value[0])
        aqi_str = aqi_cat_str + aqi_num_str[len(aqi_cat_str):6]

        return aqi_str


    # This is an error condition, so if you see this string on the clock
    #   face something went wrong
    return "777777"


def updateDisplay(IO_list, to_display):
    """ This function updates the tube display. It takes two inputs, the list
    of IOs corresponding to the output of the initGPIOs function, and a string
    to_display, which should be a six character numerical string with each
    character corresponding to a digit on the display. This function does not
    return anything. """

    # The string should be exactly six characters (no more, no less), so
    #   let's verify that
    assert len(to_display) == 6

    # Display every digit from left to right
    for i in range(0, len(to_display)):
        digit = to_display[i]

        # The string we got should be of the format "DDDDDD", where every digit
        #  D is in the range of 0, 9 inclusive. Let's make sure that's the case
        try:
            int_digit = int(digit)
        except ValueError:
            # If it's not a digit from 0 - 9, let's see if it is A to see if
            #   it should be turned off
            if digit == "A":
                int_digit = 15        # Anything above 9 turns the tube off
            else:
                raise ValueError("Nixie tubes can only display digits from "\
                        "0 - 9 inclusive!")

        # Get the binary representation of the digit as a list from MSB to LSB
        #  filled with leading zeroes so that it is four characters long
        binary_digit_str = '{0:0b}'.format(int_digit).zfill(4)
        digit_binary = [int(c) for c in list(binary_digit_str)]

        # Now each of these digits corresponds to an IO which is tied to a
        #   decoder on the PCB, let's drive them accordingly
        for k in range(0, len(digit_binary)):
            GPIO.output(IO_list[i][k], digit_binary[k])



def Main():
    """ This is the main loop for the program. Variables are initialized
    and then the loop runs indefinitely unless any Exception is encountered.
    GPIO cleanup occurs at the end so that the tubes are not left on.

    Please see my website, http://www.danielseabra.net, for a more
    detailed description of the procedural logic in the main loop. """

    # Initialize variables
    button_press_timer = BUTTON_PRESS_INIT_VAL
    button_hold_timer = BUTTON_HOLD_INIT_VAL
    pir_timer = PIR_DETECT_INIT_VAL
    pir_holdoff = 0

    # Start the clock in the ALTERNATE mode
    current_mode = Mode.ALTERNATE

    # Personal preference - select your temperature unit here
    temp_unit = Temp_Units.CELSIUS

    # Clock does not start asleep
    is_asleep = False

    # Get list of GPIOs connected to the decoders driving the nixie tubes
    IO_list = initGPIOs()

    # Get current weather
    temp = getTemperatureAPI(temp_unit)
    (aqi_cat, aqi_num) = getAQIAPI()
    last_weather_time = datetime.datetime.now()

    # Get the current room temperature
    int_temp = getRoomTemp(temp_unit)
    last_int_temp_time = datetime.datetime.now()

    time_counter = datetime.datetime.now()

    try:
        while True:
            # Check and process the button status
            button_press_timer, button_hold_timer, button_pressed, button_held = \
                    checkButton(button_press_timer, button_hold_timer)

            if button_pressed:
                # The button was reported as being pressed, so let's move on
                #   to the next mode

                current_mode += 1

                if current_mode >= len(Mode):
                    current_mode = 0

            if button_held:
                # The button has been held down, so let's put the display to
                #   sleep or wake it up
                is_asleep = not is_asleep


            # Get current time
            current_time = datetime.datetime.now()

            # Check and process the PIR sensor status
            pir_timer, pir_timedout = checkPIR(pir_timer)

            if pir_timedout:
                is_asleep = True
            else:
                is_asleep = False

            # Update temperature variables if it's time to do so
            if current_mode == Mode.TEMP or current_mode == Mode.ALTERNATE \
                    or current_mode == Mode.AQI:
                # Update current temperature if it's been more than
                #   TEMP_UPDATE_SECONDS since we've last done it
                td = current_time - last_weather_time

                if td.seconds > EXT_TEMP_UPDATE_SECONDS:
                    last_weather_time = datetime.datetime.now()
                    temp = getTemperatureAPI(temp_unit)
                    (aqi_cat, aqi_num) = getAQIAPI()

                td = current_time - last_int_temp_time
                if td.seconds > INT_TEMP_UPDATE_SECONDS:
                    last_int_temp_time = datetime.datetime.now()
                    int_temp = getRoomTemp(temp_unit)

            # If we're in ALTERNATE mode, pick which display is going to be
            #   shown this cycle
            #   NOTE: Change this logic to suit your needs/desires for how
            #       you want the clock to behave
            disp_mode = current_mode
            value_displayed = current_time

            time_diff_s = (current_time - time_counter).seconds
            if (time_diff_s >= 70):
                time_counter = current_time
                time_diff_s = 0

            if disp_mode == Mode.ALTERNATE:
                if time_diff_s < 20:
                    disp_mode = Mode.TIME
                elif time_diff_s < 40:
                    disp_mode = Mode.TEMP
                    value_displayed = (int_temp, temp)
                elif time_diff_s < 50:
                    disp_mode = Mode.TEMP
                    value_displayed = (int(round(celsiusToF(int_temp))),
                            int(round(celsiusToF(temp))))
                else:
                    disp_mode = Mode.AQI
                    value_displayed = (aqi_cat, aqi_num)


            # Get the string to display
            to_display = getDisplayString(disp_mode, is_asleep, value_displayed)

            # And now we're going to go ahead and display that string
            updateDisplay(IO_list, to_display)

            # And sleep for SLEEP_TIME milliseconds
            time.sleep(SLEEP_TIME / 1000.)

    except Exception as e:
        print(e)

    finally:
        # This is good practice, otherwise the GPIOs will be left driving
        #   whatever Voltages they were before, and the tubes will be left
        #   on
        GPIO.cleanup()

if __name__ == "__main__":
    Main()
