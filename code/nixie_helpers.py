import os
from enum import Enum
from gpiozero import CPUTemperature
import json
from urllib.request import urlopen

# Constants
Temp_Units = Enum('Temp_Units', 'CELSIUS FAHRENHEIT')

# WEATHER API KEY (please don't use my key!)
WEATHER_API_KEY = "THIS IS AN API KEY"
# AQI API KEY (please don't use my key!)
AIRNOW_AQI_API_KEY = "THIS IS AN API KEY"

# Full API call to get the weather. Change id=xxxxx for your city code, this
#   is for Santa Clara, CA
WEATHER_API_CALL_STR = "https://api.openweathermap.org/data/2.5/" + \
        "weather?id=5392171&appid=" + WEATHER_API_KEY

# Full API call to get the AQI. Change zipCode=xxxxx for your zip code,, this
#   is for Santa Clara, CA
AQI_API_CALL_STR = "http://www.airnowapi.org/aq/observation/zipCode/current/" + \
        "?format=application/json&zipCode=95051&distance=25&API_KEY=" + \
        AIRNOW_AQI_API_KEY

# Temperature offset in Fahrenheit between CPU and room temperature (calibrated
#   with a thermometer)
ROOM_TEMP_OFFSET = 34


def kelvinToC(degree):
    """ Converts Kelvin to degrees Celsius """
    return degree - 273

def celsiusToF(degree):
    """ Converts degrees Celsius to degrees Fahrenheit """
    return 9/5 * degree + 32

def fahrenheitToC(degree):
    """ Converts degrees Fahrenheit to degrees Celsius """
    return (degree - 32) * 5/9

def getTemperatureAPI(unit):
    """ Gets the temperature (in the unit corresponding to unit) from the
    OpenWeatherMap API """
    webstr = urlopen(WEATHER_API_CALL_STR).read()
    webstr = webstr.decode('utf-8')
    weather = json.loads(webstr)
    temp = int(round(kelvinToC(weather['main']['temp'])))

    if unit == Temp_Units.FAHRENHEIT:
        temp = int(round(celsiusToF(temp)))

    return temp

def getAQIAPI():
    """ Gets the AQI and the AQI category from the AirNow API """
    try:
        webstr = urlopen(AQI_API_CALL_STR).read()
        webstr = webstr.decode('utf-8')
        aqi = json.loads(webstr)
        aqi_num = aqi[1]['AQI']
        aqi_cat = aqi[1]['Category']['Number']
    except:
        aqi_cat = 0
        aqi_num = 0

    return (aqi_cat, aqi_num)

def getCPUTemp(unit):
    """ Gets the CPU temperature """
    temp = os.popen('vcgencmd measure_temp').readline()
    temp = float(temp.replace("temp=","")[0:-3])

    if unit == Temp_Units.CELSIUS:
        return int(round(temp))

    return int(round(celsiusToF(temp)))

def getRoomTemp(unit):
    cpu_temp = getCPUTemp(Temp_Units.FAHRENHEIT)

    if unit == Temp_Units.FAHRENHEIT:
        return cpu_temp - ROOM_TEMP_OFFSET
    else:
        return int(round(fahrenheitToC(cpu_temp - ROOM_TEMP_OFFSET)))

